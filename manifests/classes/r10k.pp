sshkey { 'gitlab.com':
ensure => present,
type   => 'ssh-rsa',
target => '/root/.ssh/known_hosts',
key    => '...+dffsfHQ==',
}

class { 'r10k':
  configfile => '/etc/puppetlabs/r10k/r10k.yaml',
  sources => {
    'puppet' => {
      'remote'  => 'git@gitlab.com:GW-DevOps-group/projeto_GITA.git',
      'basedir' => "${::settings::environmentpath}",
      'prefix'  => true,
    }
  },
}
