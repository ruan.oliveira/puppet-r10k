include apache
class { '::apache':
  default_mods => [
    'info',
    'alias',
    'mime',
    'env',
    'setenv',
    'expires',
    'worker',
    'php',
    'userdir',
  ],
  mpm_module => 'worker',
}
