include nginx

nginx::vhost { 'bancodobrasil.puppetlabs.vm': }

nginx::vhost { 'rnp.puppetlabs.vm':
  port => '82',
}

nginx::vhost { 'dataprev.puppetlabs.vm':
  port => '7000',
}

nginx::vhost { 'sicoob.puppetlabs.vm': }

nginx::vhost { 'conix.puppetlabs.vm': }

nginx::vhost { 'globalweb.puppetlabs.vm':
  port => '666',
}

nginx::vhost { 'instruct.puppetlabs.vm': }
