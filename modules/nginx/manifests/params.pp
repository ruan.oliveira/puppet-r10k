class nginx::params {

  $treinamento = 'Fundamentals'
  $cidade = 'Brasilia'
  $highperf = true

  case $facts['os']['family'] {
    'RedHat': {
      $package_name = 'nginx'
      $owner = 'root'
      $group = 'root'
      $mode = '0644'
      $docroot = '/srv/sites'
      $confdir = '/etc/nginx'
      $blockdir = '/etc/nginx/conf.d'
      $logdir = '/var/log/nginx'
      $service_name = 'nginx'
      $service_user = 'nginx'
    }
    'Debian': {
      $package_name = 'nginx'
      $owner = 'root'
      $group = 'root'
      $mode = '0644'
      $docroot = '/srv/sites'
      $confdir = '/etc/nginx'
      $blockdir = '/etc/nginx/conf.d'
      $logdir = '/var/log/nginx'
      $service_name = 'nginx'
      $service_user = 'www-data'
    }
    'windows': {
      $package_name = 'nginx-service'
      $owner = 'Administrator'
      $group = 'Administrators'
      $mode = '0644'
      $docroot = 'C:/ProgramData/nginx/html/sites'
      $confdir = 'C:/ProgramData/nginx/conf'
      $blockdir = 'C:/ProgramData/nginx/conf.d'
      $logdir = 'C:/ProgramData/nginx/logs'
      $service_name = 'nginx'
      $service_user = 'nobody'
    }
    default: {
        fail("Operating system family ${facts['os']['family']} is not supported.")
    }
  }
}
