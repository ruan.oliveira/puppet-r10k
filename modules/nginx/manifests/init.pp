class nginx (
  Boolean $highperf    = $nginx::params::highperf,
  String $package_name = $nginx::params::package_name,
  String $owner        = $nginx::params::owner,
  String $group        = $nginx::params::group,
  String $mode         = $nginx::params::mode,
  String $docroot      = $nginx::params::docroot,
  String $confdir      = $nginx::params::confdir,
  String $blockdir     = $nginx::params::blockdir,
  String $logdir       = $nginx::params::logdir,
  String $service_name = $nginx::params::service_name,
  String $service_user = $nginx::params::service_user,
  ) inherits nginx::params {

  File {
    owner => $owner,
    group => $group,
    mode  => $mode,
  }

  package { $package_name:
    ensure => installed,
  }

  file { $docroot:
    ensure => directory,
  }

  file { "${confdir}/nginx.conf":
    ensure  => file,
    content => epp('nginx/nginx.conf.epp',{
      service_user => $service_user,
      logdir       => $logdir,
      confdir      => $confdir,
      blockdir     => $blockdir,
      highperf     => $highperf,
      }),
    notify  => Service[$service_name],
  }

  nginx::vhost { 'default': }

  service { $service_name:
    ensure => running,
    enable => true,
  }

}
