define nginx::vhost (
  $port = '80',
  $docroot = "${nginx::docroot}/${title}",
  $servername = $title,
  ) {

  host { $servername:
    ip => $facts[networking][ip]
  }

  file { $docroot:
    ensure => directory,
  }

  file { "${docroot}/index.html":
    ensure  => file,
    content => epp('nginx/index.html.epp',{
      servername => $servername,
      }),
  }

  file { "${nginx::blockdir}/${title}.conf":
    ensure  => file,
    content => epp('nginx/vhost.conf.epp',{
      docroot    => $docroot,
      port       => $port,
      servername => $servername,
      }),
    notify  => Service[$nginx::service_name],
  }

}
